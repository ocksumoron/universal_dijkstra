//
//  main.cpp
//  universal_dijkstra
//
//  Created by Даня on 25.05.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#include <iostream>
#include <vector>
#include <set>
#include <climits>
#include <string>

typedef unsigned int ui;

using std::vector;
using std::set;
using std::cin;
using std::cout;
using std::make_pair;
using std::pair;

class IEdge {
public:
    ui start, finish;
    ~IEdge() {}
};

template <typename DistanceType>
class VertexDistance {
public:
    ui index;
    DistanceType distance;
    VertexDistance(ui _index, DistanceType _distance): index(_index), distance(_distance) {}
    ~VertexDistance() {}
};

template <typename DistanceType, typename EdgeType>
class IDistanceCalculator {
public:
    virtual DistanceType getZeroDistance() = 0;
    virtual DistanceType getInfDistance() = 0;
    virtual DistanceType calculateDistance(DistanceType distanceToEdgeStart, EdgeType* edge) = 0;
    virtual ~IDistanceCalculator() {}
};

template <typename DistanceType, typename EdgeType>
class Graph {
private:
    IDistanceCalculator<DistanceType, EdgeType>* distanceCalculator;
    ui vertexesNumber;
public:
    vector<vector <EdgeType*> > adjacencyList;
    Graph (IDistanceCalculator<DistanceType, EdgeType>* _distanceCalculator, ui _vertexesNumber): distanceCalculator(_distanceCalculator), vertexesNumber(_vertexesNumber),
    adjacencyList(vector<vector <EdgeType*> >(_vertexesNumber)) {}
    
    void addEdge(EdgeType* newEdge) {
        adjacencyList[newEdge->start].push_back(newEdge);
    }
    
    ui getVertexesNumber() {
        return vertexesNumber;
    }
    
    IDistanceCalculator<DistanceType, EdgeType>* getDistanceCalculator() {
        return distanceCalculator;
    }
    
    ~Graph() {
        if (distanceCalculator)
            delete distanceCalculator;
    }
};

template <typename DistanceType, typename EdgeType, class Compare>
vector <DistanceType> dijkstra(Graph<DistanceType, EdgeType>* graph, ui startVertex, Compare comp) {
    vector <DistanceType> distances(graph->getVertexesNumber(), graph->getDistanceCalculator()->getInfDistance());
    distances[startVertex] = graph->getDistanceCalculator()->getZeroDistance();
    set <VertexDistance<DistanceType>, Compare> distancesSet;
    distancesSet.insert(VertexDistance<DistanceType>(startVertex, distances[startVertex]));
    while (!distancesSet.empty()) {
        VertexDistance<DistanceType> currentVertex = *distancesSet.begin();
        distancesSet.erase(distancesSet.begin());
        for (ui i = 0; i < graph->adjacencyList[currentVertex.index].size(); ++i) {
            EdgeType* currentEdge = graph->adjacencyList[currentVertex.index][i];
            ui nextVertex = currentEdge->finish;
            DistanceType nextDistance = graph->getDistanceCalculator()->calculateDistance(currentVertex.distance, currentEdge);
            if (comp(nextDistance, distances[nextVertex])) {
                distancesSet.erase(VertexDistance<DistanceType>(nextVertex, distances[nextVertex]));
                distances[nextVertex] = nextDistance;
                distancesSet.insert(VertexDistance<DistanceType>(nextVertex, distances[nextVertex]));
            }
        }
    }
    return distances;
}

class StandardEdge: public IEdge {
public:
    ui start, finish, weight;
    StandardEdge(ui _start, ui _finish, ui _weight): start(_start), finish(_finish), weight(_weight) {}
    ~StandardEdge() {}
};

class StandardComparator {
public:
    bool operator()(const ui &a, const ui &b) const {
        return a < b;
    }
    bool operator()(const VertexDistance<ui> &a, const VertexDistance<ui> &b) const {
        return a.distance < b.distance || (a.distance == b.distance && a.index < b.index);
    }
};

class StandardDistanceCalculator: public IDistanceCalculator<ui, StandardEdge> {
    
    ui getZeroDistance() {
        return 0;
    }
    
    ui getInfDistance() {
        return 2009000999;
    }
    
    ui calculateDistance(ui distanceToEdgeStart, StandardEdge* edge) {
        if (distanceToEdgeStart >= getInfDistance())
            return getInfDistance();
        return distanceToEdgeStart + edge->weight;
    }
    
    ~StandardDistanceCalculator() {}
};

void standardSolution() {
    int NUM, n, m, start, finish, weight;
    StandardDistanceCalculator* standardDistanceCalculator = new StandardDistanceCalculator();
    StandardComparator standardComparator;
    cin >> NUM;
    for (ui t = 0; t < NUM; ++t) {
        cin >> n >> m;
        Graph<ui, StandardEdge>* graph = new Graph<ui, StandardEdge>(standardDistanceCalculator, n);
        for (ui i = 0; i < m; ++i) {
            cin >> start >> finish >> weight;
            StandardEdge* edge = new StandardEdge(start, finish, weight);
            graph->addEdge(edge);
            edge = new StandardEdge(finish, start, weight);
            graph->addEdge(edge);
        }
        cin >> start;
        vector <ui> currentAnswer = dijkstra(graph, start, standardComparator);
        //        vector <ui> currentAnswer = dijkstra(graph, start, standardComparator);
        for (int i = 0; i < n; ++i) {
            cout << currentAnswer[i] << " ";
        }
        cout << "\n";
    }
    return;
}

class BusRouteEdge: public IEdge {
public:
    ui start, finish, departureTime, arrivalTime;
    BusRouteEdge(ui _start, ui _finish, ui _departureTime, ui _arrivalTime): start(_start), finish(_finish),
    departureTime(_departureTime), arrivalTime(_arrivalTime) {}
    ~BusRouteEdge() {}
};

class BusRouteDistanceCalculator: public IDistanceCalculator<ui, BusRouteEdge> {
public:
    
    ui getZeroDistance() {
        return 0;
    }
    
    ui getInfDistance() {
        return UINT_MAX;
    }
    
    ui calculateDistance(ui distanceToEdgeStart, BusRouteEdge* edge) {
        if (edge->departureTime < distanceToEdgeStart)
            return getInfDistance();
        return edge->arrivalTime;
    }
};

void BusesSolution() {
    int n, m, start, finish, departure, arrival, d, v;
    cin >> n;
    cin >> d >> v;
    BusRouteDistanceCalculator* routeDistanceCalculator = new BusRouteDistanceCalculator();
    Graph<ui, BusRouteEdge>* graph = new Graph<ui, BusRouteEdge>(routeDistanceCalculator, n);
    StandardComparator standardComparator;
    cin >> m;
    for (int i = 0; i < m; ++i) {
        cin >> start >> departure >> finish >> arrival;
        BusRouteEdge* edge = new BusRouteEdge(start - 1, finish - 1, departure, arrival);
        graph->addEdge(edge);
    }
    vector <ui> answer = dijkstra(graph, d - 1, standardComparator);
    if (answer[v - 1] == UINT_MAX) {
        cout << -1;
    } else {
        cout << answer[v - 1];
    }
    return;
}

class ShipmentEdge: public IEdge {
public:
    ui start, finish, length, maxWeight;
    ShipmentEdge(ui _start, ui _finish, ui _length, ui _maxWeight): start(_start), finish(_finish),
    length(_length), maxWeight(_maxWeight) {}
    ~ShipmentEdge() {}
};

class ShipmentDistance {
public:
    ui distance;
    ui weight;
    ShipmentDistance(ui _distance, ui _weight): distance(_distance), weight(_weight) {}
};

class ShipmentDistanceCalculator: public IDistanceCalculator<ui, ShipmentEdge> {
    ui maxWeight;
public:
    
    void setMaxWeight(ui _maxWeight) {
        maxWeight = _maxWeight;
    }
    
    ui getZeroDistance() {
        return 0;
    }
    
    ui getInfDistance() {
        return UINT_MAX;
    }
    
    ui calculateDistance(ui distanceToEdgeStart, ShipmentEdge* edge) {
        if (maxWeight > edge->maxWeight)
            return getInfDistance();
        else
            return distanceToEdgeStart + edge->length;
    }
};

void ShipmentSolution() {
    ui n, m, l, r, mid, start, finish, length, weight;
    bool flag;
    vector <ui> answer;
    cin >> n >> m;
    ShipmentDistanceCalculator* shipmentDistanceCalculator = new ShipmentDistanceCalculator();
    Graph<ui, ShipmentEdge>* graph = new Graph<ui, ShipmentEdge>(shipmentDistanceCalculator, n);
    StandardComparator standardComparator;
    for (int i = 0; i < m; ++i) {
        cin >> start >> finish >> length >> weight;
        ShipmentEdge* edge = new ShipmentEdge(start - 1, finish - 1, length, weight);
        graph->addEdge(edge);
        edge = new ShipmentEdge(finish - 1, start - 1, length, weight);
        graph->addEdge(edge);
    }
    l = 3000000;
    r = 1003000010;
    while (r - l > 1) {
        mid = (l + r) >> 1;
        shipmentDistanceCalculator->setMaxWeight(mid);
        answer = dijkstra(graph, 0, standardComparator);
        flag = answer[n - 1] <= 1440;
        if (flag)
            l = mid;
        else
            r = mid;
    }
    cout << (l - 3000000) / 100;
    return;
}

int main() {
    return 0;
}
